from socket import *
import pickle
import sys
import time

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
#PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

ORACLE_IP		= "127.0.0.1"
ORACLE_PORT		= 11111
ORACLE 			= (ORACLE_IP, ORACLE_PORT)
#-------------------------------------------------------------------------------
def send_data(out_sock, string_data):
	out_sock.send( str(len(string_data)).ljust(16))
	out_sock.send( string_data )
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def echo_to_cml(port):
    print('Listening on %s:%d' % (HOST, port))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, port))
    s.listen(5)
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            print data
#-------------------------------------------------------------------------------
def get_server_ip_from_oracle(service):
	''' This goes to the oracle to get the current ip for the server
	'''
	msg_type 	= "lookup"
	raw_data 	= pickle.dumps(service)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	length 		= int(recv_all(s, 16))
	raw_data  	= recv_all(s, length)
	server 		= pickle.loads(raw_data)
	print(server)

	s.close()

	print("Current server for '%s' is '%s'" % (service, server))
	return server
#-------------------------------------------------------------------------------
def generate_traffic(service_name):

	reset_count = 20

	while True:
		#connect to broker and get the port that we want
		server = get_server_ip_from_oracle(service_name)

		if server is None:
			print 'No address for service ', service_name
			return

		# create the connection
		s = socket(AF_INET, SOCK_STREAM)
		s.connect(server)

		data  = "Hello"
		count = 0

		# now send some data
		while True:
			send_data(s, data)

			count += 1
			if count > reset_count:
				break
			time.sleep(0.1)
#-------------------------------------------------------------------------------
if __name__ == "__main__":

    if len(sys.argv) < 2:
        print 'Usage python traffic_gen.py <service name>'
    else:
        generate_traffic(str(sys.argv[1]))