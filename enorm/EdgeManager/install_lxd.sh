#!/bin/bash

echo '---------------------------------------------------------'
echo 'This will install the lxd version of the linux containers'
echo '---------------------------------------------------------'
sudo apt install -y lxd lxd-client

echo '--------------------------------------'
echo 'Now init LXD - defaults should be fine'
echo '--------------------------------------'
sudo lxd init