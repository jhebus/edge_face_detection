#!/bin/bash

LXC=$1
DECISION=$2
#CPU_UNIT=1000000 # represent 1 out of 8 cores
CPU_UNIT=1
MEM_UNIT=256 # MB

OLD_CPU=$(lxc config get $LXC limits.cpu)
OLD_MEM=$(lxc config get $LXC limits.memory)
OLD_MEM=${OLD_MEM//MB}

MAX_CPU_LIMIT=$((CPU_UNIT * 8))
MAX_MEM_LIMIT=$((MEM_UNIT * 8))

#echo "$OLD_CPU || $MAX_CPU_LIMIT"
#echo "$OLD_MEM || $MAX_MEM_LIMIT"

case $DECISION in 
"up")
	# Abort if LXC is using the maximum resource
	if (($OLD_CPU >= $MAX_CPU_LIMIT)) || (($OLD_MEM >= $MAX_MEM_LIMIT))
	then
		echo "Scaling up aborted as maximum resource reached."
	else
		NEW_CPU=$(($OLD_CPU + $CPU_UNIT))
		NEW_MEM=$(($OLD_MEM + $MEM_UNIT))

		lxc config set $LXC limits.cpu $NEW_CPU
		lxc config set $LXC limits.memory $NEW_MEM"MB"

		echo "$LXC scaled up. New CPU: $NEW_CPU, new memory: $NEW_MEM; old CPU:$OLD_CPU, old memory:$OLD_MEM"
	fi
		;;
"down")
	# Abort if LXC is using the minimum resource
	if (($OLD_CPU <= $CPU_UNIT )) || (($OLD_MEM <= $MEM_UNIT))
	then
		echo "Scaling down aborted as minimum resource reached."
	else
		NEW_CPU=$(($OLD_CPU - $CPU_UNIT)) 
		NEW_MEM=$(($OLD_MEM - $MEM_UNIT))

		lxc config set $LXC limits.cpu $NEW_CPU
		lxc config set $LXC limits.memory $NEW_MEM"MB"

		echo "$LXC scaled down. New CPU: $NEW_CPU, new memory: $NEW_MEM; old CPU:$OLD_CPU, old memory:$OLD_MEM"	
	fi	
	;;
esac
