#!/bin/bash

NAME=$1
EDGE_IP=$2
EDGE_REDIS_PORT=$3 # This is the first port in request.txt, used by redis DB
EDGE_PORT=$4 # This is the last port in request.txt, which is updated when Edge service is setup
EDGE_SSH_PORT=$5


APP_REPO=https://jhebus@bitbucket.org/jhebus/edge_face_detection.git

#ssh -tt -i edge.key -o StrictHostKeyChecking=no root@$EDGE_IP -p $EDGE_SSH_PORT \
#"apt install -y python python-opencv ;" \
#"cd $NAME-edgeserver ;" \
#"tmux ; " \
#"python to_greyscale.py 2>&1 serverLog & &&" \
#"tmux detach"

ssh -i edge.key -o StrictHostKeyChecking=no root@$EDGE_IP -p $EDGE_SSH_PORT << EOF
apt install -y python python-opencv ;
git clone $APP_REPO $NAME-edgeserver ;
cd $NAME-edgeserver ;
(nohup python to_greyscale.py > serverLog) & 
exit
EOF

#update the oracle for the location
echo $NAME $EDGE_IP $EDGE_PORT
python /home/paul/repo/edge_face_detection/oracle_client.py register -n $NAME -ip $EDGE_IP -p $EDGE_PORT

python /home/paul/repo/edge_face_detection/broker_client.py register --in_port $EDGE_PORT -ip 127.0.0.1 -prt 8000
