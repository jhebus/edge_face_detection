from socket import *
import pickle
import time
import os
import stat

PORT = 22221
#EDGE_IP = '172.21.0.65'
EDGE_IP = '127.0.0.1'

PATH = "/home/paul/repo/edge_face_detection/enorm/CloudManager"
# Request from args to use edge service.
# e.g.
# {'App':'iPokeMon' , 'Ports':[6379,8000], 'Premium': '1', 'Objective': '80'}

#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
def read_request(inFile):
	""" Read request from file into a dictionary"""
	req = {}
	with open(inFile) as f:
    		for line in f:
			kv = line.split()			
        		k, v = kv[0], kv[1:]
       			req[k] = v
	return req
#-------------------------------------------------------------------------------
def parse_response(data):
    data_arr = pickle.loads(data)
    return data_arr
#-------------------------------------------------------------------------------
def greeting(server):
	server.send("hello")
	print "Hello to Edge..."
	response = server.recv(1024)
	return response
#-------------------------------------------------------------------------------
def request_edge_service(request, server):
	request_str = pickle.dumps(request)
	server.send(request_str)
	print "Request sent."

	length 		= int(recv_all(server, 16))
	raw_data 	= recv_all(server, length)
	return pickle.loads(raw_data)
#-------------------------------------------------------------------------------	
def recv_key(server):
	
	print "Receiving access key..."
	'''
	f = open('edge.key','wb')
	l = server.recv(1024)
	while (l):
		f.write(l)
		l = server.recv(1024)
	f.close()
	'''
	length 		= int(recv_all(server, 16))
	raw_data 	= recv_all(server, length)

	print("key size : %d" % length)
	print(raw_data)

	with open('edge.key', 'wb') as f:
		f.write(raw_data) 
		f.close()

	#subprocess.call(['chmod', '0400', 'edge.key'])
	os.chmod('edge.key', stat.S_IRUSR)

	print "Access key received."
#-------------------------------------------------------------------------------
def send_request(edge_ip, request_file):
	s = socket(AF_INET,SOCK_STREAM)
	s.connect((edge_ip, PORT))
	greeting_response = greeting(s)

	if greeting_response == "hello":
		request = read_request(request_file)
		reply   = request_edge_service(request, s)

		if reply == "Reject":
			print "Request is rejected. Try again later."
		else: 
			print "Edge ports config: %s" % reply
			recv_key(s)
			print "Edge service container setup. Start deploying App..."
			s.close()
			print(request)
			print(reply)
			os.system('%s/deployApp.sh %s %s %s %s %s' % (PATH, request['App'][0], EDGE_IP, reply[0], reply[1], reply[2]))
	else:
		print "This Edge node is not available. Try again later."
		s.close()
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	send_request(EDGE_IP, "request.txt")
