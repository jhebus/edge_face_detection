import socket
import sys
import os
import pickle

CLOUD_IP 	= "172.22.0.31"
CLOUD_PORT	= 22221
CLOUD 		= (CLOUD_IP, CLOUD_PORT)

EDGE_MANAGER_PORT	= 22221

ORACLE_IP		= "127.0.0.1"
ORACLE_PORT		= 11111
ORACLE 			= (ORACLE_IP, ORACLE_PORT)

BROKER_IP	= "127.0.0.1"
BROKER_PORT	= 11112
BROKER		= (BROKER_IP, BROKER_PORT)

#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
def get_server_ip_from_oracle(service):
	''' This goes to the oracle to get the current ip for the server
	'''
	msg_type 	= "lookup"
	raw_data 	= pickle.dumps(service)

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	length 		= int(recv_all(s, 16))
	raw_data  	= recv_all(s, length)
	server 		= pickle.loads(raw_data)
	#print(server)

	s.close()

	print("Current server for '%s' is '%s'" % (service, server))
	return server
#-------------------------------------------------------------------------------
def remove_service_from_oracle(name):
	msg_type = "remove"

	service 	= name
	raw_data 	= pickle.dumps(service)

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("remove complete")
#-------------------------------------------------------------------------------
def remove_service_from_broker(in_port):
	msg_type = "remove"

	entry = {}
	entry['msg_type']	= msg_type
	entry['in_port'] 	= in_port
	
	raw_data 			= pickle.dumps(entry)

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(BROKER)

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("remove complete")
#-------------------------------------------------------------------------------
def test_update(app):
	msg_type = "update"

	entry = {}
	entry['name'] 	= app
	entry['ip'] 	= CLOUD_IP
	entry['port']	= CLOUD_PORT

	raw_data 		= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("update complete")
#-------------------------------------------------------------------------------
#TODO: this redirect first...
def send_request(app, edge_server):
	edge_manager = (edge_server[0], EDGE_MANAGER_PORT)

	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.connect(edge_manager)
	s.send("terminate %s" % app)
	response = s.recv(1024)
	if response.split()[0] == "Terminated":
		# redirect user to cloud
		#os.system('. redirectUsers.sh %s' % CLOUD_IP)
		remove_service_from_oracle(app)
		remove_service_from_broker(edge_server[1])
		
	else:
		print response
		print("The above does not exist....")
	s.close()
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	app = sys.argv[1]

	edge_server = get_server_ip_from_oracle(app)

	if edge_server is None:
		print('There is no registered service for %s' % app)
	else:
		send_request(app, edge_server)
