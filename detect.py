#Import OpenCV for image processing
#Import Time
import cv2, time

# For the networking
from socket import *


import threading
import numpy as np

#SOCK_TYPE	= SOCK_DGRAM
SOCK_TYPE	= SOCK_STREAM

#Import entire tkinter for GUI widget creation
from tkinter import *

#create an empty GUI window
window=Tk()	

IN_PORT 	= 22222

def recv_all(sock, count):
	''' Receive up to count bytes from the specified socket and return the data
	'''
	buf = b''
	while count:
		newbuf 	= sock.recv(count)
		if not newbuf: return None
		buf 	+= newbuf
		count 	-= len(newbuf)
	return buf

def receive_frame(conn):
	length 		= int(recv_all(conn,16))
	stringData 	= recv_all(conn, length)
	data 		= np.fromstring(stringData, dtype='uint8')
	frame 		= cv2.imdecode(data,1)

	# debug
	#cv2.imshow('FACE_SERVER',frame)
	#cv2.waitKey(10)
	
	return frame

#This is the function that gets triggered when you press the button on your widget.
#The function exits when you press Q on the keyboard
def find_face(client, addr):

	#Cascade Classifier
	#https://docs.opencv.org/2.4/modules/objdetect/doc/cascade_classification.html
	#The Cascade work on the principle that the classifier increasingly 
	#considers more features for the detection of faces
	#Starting off with simpler classifiers it increasingly uses them in combination 
	#for subsequent steps if the prior steps yield positive results
	#The Haar Cascade Classifier contains pre-trained classication data in XML files
	#In our case we use the Frontal Face Cascade Classifier
	#This file must be available for the purpose of classification
	
	faceCascade=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")


	#continuously processes video feed and displays in window until Q is pressed
	while True:
		
		# read grey image
		# read the ada from the addr provided
		grey_img = receive_frame(client)
		
		#This is the crux of the program where discretionary parameters can be set for better results
		
		#The Scalefactor decides how many iterations the Classifier will perform in its seach for images. 
		#In every iteration, the Classifier looks for a bigger face size, starting from small and going up
		#until it is checking the entire size of the frame. The scaleFactor decides this step size. 
		#Smaller the scaleFactor, more sensitive the Classifier and longer it takes to process the frame
		
		#The minNeighbors decides the Parameter specifying how many neighbors each candidate rectangle should have to retain it.
		#Greater the minNeighbors, greater the sensitivity. However, higher might lead to false positives.
		
		#The detectMultiScale returns a list of (set of) values that can be used to build a rectangle as shown below
		faces=faceCascade.detectMultiScale(grey_img, scaleFactor=1.05, minNeighbors=5)
		
		#superimposes a rectangle for all the detected images in the frame
		#(x,y) is the top left corner, (x+w, y+h) is the bottom right corner
		#(0,255,0) is colour green and 3 is the thickness of the rectangle edges
		for x, y, w, h in faces:
			frame=cv2.rectangle(grey_img, (x,y), (x+w, y+h), (0,255,0), 3)

		if len(faces) == 0:
			print("No face")

		#window displaying the processed image
		cv2.imshow("FACE_SERVER", grey_img)

		#picks up the key press Q and exits when pressed
		key=cv2.waitKey(1)

	
	#Closes video window
	cv2.destroyAllWindows()
	

def show_face(client, addr):
	#continuously processes video feed and displays in window until Q is pressed
	while True:
		
		# read grey image
		# read the ada from the addr provided
		img = receive_frame(client)
		
		#window displaying the processed image
		cv2.imshow("FACE_SERVER", img)

		#picks up the key press Q and exits when pressed
		key=cv2.waitKey(1)

	
	#Closes video window
	cv2.destroyAllWindows()

def main():
	in_sock = socket()	
	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', IN_PORT))
	in_sock.listen(5)

	print 'Listening on:', IN_PORT
	while True:
		conn, addr = in_sock.accept()
		print 'Connection from:', addr
		#threading.Thread(target=find_face, args=(conn,addr)).start()
		threading.Thread(target=show_face, args=(conn,addr)).start()

if __name__ == '__main__':
	main();
