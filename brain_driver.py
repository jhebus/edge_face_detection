from socket import *
import threading
import time
import pickle
import sys
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_reports(interval, brain_server, location, service):
	
	entry = {}
	entry['msg_type']	= "oracle_report"
	entry['location'] 	= location
	entry['service'] 	= service

	raw_data = pickle.dumps(entry)
	count = 0

	while True:
		print 'REPORT ', count
		count += 1

		out_sock = socket(AF_INET, SOCK_STREAM)
		out_sock.connect(brain_server)

		out_sock.send(str(len(raw_data)).ljust(16))
		out_sock.send(raw_data)

		out_sock.close()

		time.sleep(interval)

#-------------------------------------------------------------------------------
def generate_eject(service, brain_server, deploy_location, interval, key):
	entry = {}
	entry['msg_type']			= "eject_from_edge"
	entry['deploy_location'] 	= deploy_location
	entry['service'] 			= service
	entry['key']				= key

	raw_data = pickle.dumps(entry)
	count 	 = 0
	while True:
		print 'EJECT ', count
		count += 1

		out_sock = socket(AF_INET, SOCK_STREAM)
		out_sock.connect(brain_server)

		out_sock.send(str(len(raw_data)).ljust(16))
		out_sock.send(raw_data)

		out_sock.close()

		time.sleep(interval)	
#-------------------------------------------------------------------------------
def parse_arguments():
	print 'parse arguments'
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	print 'Started the brain driver'

	#location = sys.argv[1]
	#service  = sys.argv[2]

	location = "site1"
	service  = "srv1"
	key 	 = service + location
	server = ("127.0.0.1", 55551)
	report_interval = 0.5
	eject_interval  = 10.2

	parse_arguments()

	t 			= threading.Thread(target=generate_reports, args=(report_interval, server, location, service))
	t.daemon 	= True
	t.start()

	t 			= threading.Thread(target=generate_eject, args=(service, server, location, eject_interval, key))
	t.daemon 	= True
	t.start()

	#generate_reports(report_interval, server, location, service)

	#generate_eject(service, server, location, eject_interval)

	while True:
		time.sleep(60)