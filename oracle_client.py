# For the networking
from socket import *
import argparse
import pickle

ORACLE_IP		= "127.0.0.1"
ORACLE_PORT		= 11111
ORACLE 			= (ORACLE_IP, ORACLE_PORT)

#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
def register(name, ip, port):
	msg_type = "register"

	entry = {}
	entry['name'] 	= name
	entry['ip'] 	= ip
	entry['port']	= port

	raw_data 		= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	#length 	= int(recv_all(s, 16))
	#addr  	= recv_all(s, length)

	s.close()
	print("register complete")

#-------------------------------------------------------------------------------
def update(name, ip, port):
	msg_type = "update"

	entry = {}
	entry['name'] 	= name
	entry['ip'] 	= ip
	entry['port']	= port

	raw_data 		= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	#length 	= int(recv_all(s, 16))
	#addr  	= recv_all(s, length)

	s.close()
	print("update complete")
#-------------------------------------------------------------------------------
def lookup(name):
	msg_type = "lookup"

	service 	= name
	raw_data 	= pickle.dumps(service)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	length 		= int(recv_all(s, 16))
	raw_data  	= recv_all(s, length)
	data 		= pickle.loads(raw_data)
	print(data)

	s.close()
	print("lookup complete")
#-------------------------------------------------------------------------------
def remove(name):
	msg_type = "remove"

	service 	= name
	raw_data 	= pickle.dumps(service)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("remove complete")
#-------------------------------------------------------------------------------
def parse_args():
	'''
	register 	'app_name' 'ipaddress' 'port number'
	lookup 		'app name'
	update 		'app_name' 'ipaddress' 'port number'
	remove 		'app_name'
	'''

	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest='func')

	reg_parser = subparsers.add_parser('register')
	reg_parser.add_argument('-n', '--name', type=str, nargs=1, required=True)
	reg_parser.add_argument('-ip',  '--ip_address', type=str, nargs=1, required=True)
	reg_parser.add_argument('-prt', '--port', type=int, nargs=1, required=True)

	lookup_parser = subparsers.add_parser('lookup')
	lookup_parser.add_argument('-n', '--name', type=str, nargs=1, required=True)

	update_parser = subparsers.add_parser('update')
	update_parser.add_argument('-n', '--name', type=str, nargs=1, required=True)
	update_parser.add_argument('-ip',  '--ip_address', type=str, nargs=1, required=True)
	update_parser.add_argument('-prt', '--port', type=int, nargs=1, required=True)

	remove_parser = subparsers.add_parser('remove')
	remove_parser.add_argument('-n', '--name', type=str, nargs=1, required=True)

	args = parser.parse_args()

	print(args)

	if args.func == 'register':
	    register(args.name[0], args.ip_address[0], args.port[0])
	elif args.func == 'lookup':
		lookup(args.name[0])
	elif args.func == 'update':
		update(args.name[0], args.ip_address[0], args.port[0])
	elif args.func == 'remove':
		remove(args.name[0])
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	parse_args()
