#!/bin/bash

#This script is used to setup the rules for the forwarder

NAME=$1
IN_PORT=$2
OUT_IP=$3
OUT_PORT=$4

BROKER_IP=127.0.0.1


#eg ./setup_broker.sh echo_to_cml 45678 127.0.0.1 33331


# create a forwarder to a service
python broker_client.py register --in_port $IN_PORT -ip $OUT_IP -prt $OUT_PORT

# register this service with the oracle
python oracle_client.py register -n $NAME -ip $BROKER_IP -prt $IN_PORT

