# For the networking
from socket import *
import threading
import pickle
import sys

services = {}

MGT_PORT	= 11112

services_lock = threading.Lock()
#-------------------------------------------------------------------------------
def dump(obj, nested_level=0, output=sys.stdout):
    spacing = '   '
    if isinstance(obj, dict):
        print >> output, '%s{' % ((nested_level) * spacing)
        for k, v in obj.items():
            if hasattr(v, '__iter__'):
                print >> output, '%s%s:' % ((nested_level + 1) * spacing, k)
                dump(v, nested_level + 1, output)
            else:
                print >> output, '%s%s: %s' % ((nested_level + 1) * spacing, k, v)
                print >> output, '%s}' % (nested_level * spacing)
    elif isinstance(obj, list):
        print >> output, '%s[' % ((nested_level) * spacing)
        for v in obj:
            if hasattr(v, '__iter__'):
                dump(v, nested_level + 1, output)
            else:
                print >> output, '%s%s' % ((nested_level + 1) * spacing, v)
                print >> output, '%s]' % ((nested_level) * spacing)
    else:
        print >> output, '%s%s' % (nested_level * spacing, obj)
#-------------------------------------------------------------------------------
def dump_services():
	print '--------------------------------------------------'
	print('There are %d services' % len(services))
	for key, value in services.items():
		print('%d -> %s:%d' % (key, value[0], value[1]))

	#dump(services)
	print '--------------------------------------------------'
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return -1
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# grab data on the input, and put on the output
def fwd_pkt(in_sock, addr, in_port, out_ip, out_port):
	
	#while True:
	# output connection
	out_sock 	= socket(AF_INET, SOCK_STREAM)
	server 		= services[in_port]

	if server == None:
		print 'ERROR: could not find forwarding for in_port %d' % (in_port)
		#break
		return
		
	out_sock.connect(server)

	while True:
		# read data
		length		= int(recv_all(in_sock, 16))
		if length == -1:
			# no more data
			break
		raw_data  	= recv_all(in_sock, length)

		#print 'fwd_pkt'

		# forward data
		out_sock.send(str(length).ljust(16))
		out_sock.send(raw_data)

		#print('out_ip : %s, out_port : %d' % (out_ip, out_port))
		#dump_services()


		if services[in_port][0] != server[0] or services[in_port][1] != server[1]:
			# ip or port changed, make new connection
			print 'Change'
			print services
			
			# update the connection
			out_sock.close()
			out_sock 	= socket(AF_INET, SOCK_STREAM)
			server 		= services[in_port]

			if server == None:
				print 'ERROR: could not find forwarding for in_port %d' % (in_port)
				#break
				return
				
			out_sock.connect(server)


	out_sock.close()	


#-------------------------------------------------------------------------------
def port_listen(in_port, out_ip, out_port):

	print 'New listen thread:: %d -> %s:%d' % (in_port, out_ip, out_port)

	# input connection
	in_sock = socket(AF_INET, SOCK_STREAM)
	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', in_port))
	in_sock.listen(5)

	while True:
		conn, addr	= in_sock.accept()
		t 			= threading.Thread(target=fwd_pkt, args=(conn, addr, in_port, out_ip, out_port))
		t.daemon 	= True
		t.start()
#-------------------------------------------------------------------------------
def handle_register(conn, data):
	print data

	in_port 	= int(data['in_port'])
	out_port	= int(data['out_port'])
	out_ip		= str(data['out_ip'])

	new_reg		= in_port not in services

	#update accouting
	services_lock.acquire()
	services[in_port] = (out_ip, out_port)
	services_lock.release()

	#spawn a thread to handle forwarding
	if new_reg:
		t = threading.Thread(target=port_listen, args=(in_port, out_ip, out_port))
		t.daemon 	= True
		t.start()
	else:
		print 'Service already present for port ', in_port
		#dump(services)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def handle_remove(conn, data):
	in_port 	= int(data['in_port'])
	services_lock.acquire()
	del services[in_port]
	services_lock.release()
#-------------------------------------------------------------------------------
def process_mgt_msg(conn, addr):
	length 		= int(recv_all(conn, 16))
	raw_data  	= recv_all(conn, length)
	data 		= pickle.loads(raw_data)

	print("message is %s from %s" % (data['msg_type'], addr))

	if data['msg_type'] == 'register':
		handle_register(conn, data)
	elif data['msg_type'] == 'lookup':
		handle_lookup(conn, data)
	elif data['msg_type'] == 'update':
		handle_update(conn, data)
	elif data['msg_type'] == 'remove':
		handle_remove(conn, data)
	else:
		print("Unknown option, closing")

	dump_services()
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#TODO: save to file and restore on failure
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	in_sock = socket(AF_INET, SOCK_STREAM)
	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', MGT_PORT))
	in_sock.listen(5)

	while True:
		conn, addr = in_sock.accept()
		t = threading.Thread(target=process_mgt_msg, args=(conn,addr))
		t.daemon 	= True
		t.start()
