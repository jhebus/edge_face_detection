#!/bin/bash

OUT_IP=127.0.0.1
IN_PORT=$1
OUT_PORT=$2

python broker_client.py register --in_port $IN_PORT --ip $OUT_IP -prt $OUT_PORT