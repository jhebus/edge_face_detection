# For the networking
from socket import *
import threading
import argparse

#simple marshalling
import pickle

services = []

IN_PORT	= 11111

# The brain we report to
BRAIN 		= ("", 0)

#where we are
LOCATION 	= ""

services_lock = threading.Lock()
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#TODO: this should be udp
def report_stats(req_serv):
	entry 				= {}
	entry['msg_type']	= 'oracle_report'
	entry['location']	= LOCATION
	entry['service']	= req_serv

	global BRAIN

	print 'TODO: fill in the stats'
	print BRAIN

	raw_data 	= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(BRAIN)

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data )
	s.close()

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# The following deals with incommming requests
#-------------------------------------------------------------------------------
def handle_register(conn, data):
	svc = next((item for item in services if item['name'] == data['name']), None)
	if svc:
		print("Service %s already registered" % data['name'])
	else:
		entry = {}
		entry['name'] 	= data['name']
		entry['ip'] 	= data['ip']
		entry['port']	= data['port']

		services_lock.acquire()
		services.append(entry)
		services_lock.release()
#-------------------------------------------------------------------------------
def handle_lookup(conn, data):
	print("Lookup for %s" % data)
	svc = next((item for item in services if item['name'] == data), None)
	
	if svc:
		reply = (svc['ip'], svc['port']) 
	else:
		reply = None		

	raw_data = pickle.dumps(reply)
	conn.send(str(len(raw_data)).ljust(16))
	conn.send(raw_data)

	#report
	report_stats(data)

#-------------------------------------------------------------------------------
def handle_update(conn, data):
	svc = next((item for item in services if item['name'] == data['name']), None)

	if svc:
		svc['name'] 	= data['name']
		svc['ip'] 		= data['ip']
		svc['port']		= data['port']
	else:
		entry 			= {}
		entry['name'] 	= data['name']
		entry['ip'] 	= data['ip']
		entry['port']	= data['port']

		services_lock.acquire()
		services.append(entry)
		services_lock.release()
#-------------------------------------------------------------------------------
def handle_remove(conn, data):
	svc = next((item for item in services if item['name'] == data), None)
	if svc:
		print("Service %s will be removed" % data)
		services_lock.acquire()
		services.remove(svc)
		services_lock.release()
#-------------------------------------------------------------------------------
def process(conn, addr):
	length 		= int(recv_all(conn, 16))
	msg_type  	= recv_all(conn, length)

	print("message is %s from %s" % (msg_type, addr))

	length 		= int(recv_all(conn, 16))
	raw_data 	= recv_all(conn, length)
	data 		= pickle.loads(raw_data)

	if msg_type == 'register':
		handle_register(conn, data)
	elif msg_type == 'lookup':
		handle_lookup(conn, data)
	elif msg_type == 'update':
		handle_update(conn, data)
	elif msg_type == 'remove':
		handle_remove(conn, data)
	else:
		print("Unknown option, closing")


	print('There are %d services' % len(services))
	for item in services:
		print(item)

#-------------------------------------------------------------------------------
def parse_arguments():
	global BRAIN
	parser = argparse.ArgumentParser(description='Basically a DNS server')

	parser.add_argument('-l_prt', 	'--listen_port', type=int, nargs=1, required=True)
	parser.add_argument('-r_ip', 	'--report_ip', type=str, nargs=1, required=True)
	parser.add_argument('-r_prt', 	'--report_port', type=int, nargs=1, required=True)
	parser.add_argument('-loc', 	'--location', type=str, nargs=1, required=True)

	args= parser.parse_args()

	print args

	LOCATION 	= args.location
	IN_PORT		= args.listen_port
	BRAIN 		= (args.report_ip[0], args.report_port[0])

	print BRAIN

#-------------------------------------------------------------------------------
#TODO: save to file and restore on failure
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	parse_arguments()

	in_sock = socket(AF_INET, SOCK_STREAM)
	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', IN_PORT))
	in_sock.listen(5)

	while True:
		conn, addr = in_sock.accept()
		threading.Thread(target=process, args=(conn,addr)).start()
