#!/bin/sh

# this sets up the test environment for the toy edge compute framework

## NB for now this is just a list of things that need to be setup rather than run
## this script. Specifically need to log better

./start_oracle.sh

./start_broker.sh

./start_brain.sh
#-------------------------------------------------------------------------------

# start the service to be served by the broker
python echo_to_cml.py 33331

# start a second so we have something to switch to
python echo_to_cml.py 44441

#-------------------------------------------------------------------------------
# register the application with the broker
./setup_broker.sh echo_to_cml 45678 127.0.0.1 33331

#-------------------------------------------------------------------------------
# start the traffic generator
python traffic_gen.py echo_to_cml

#-------------------------------------------------------------------------------
# Now we can change server between things
./change_server.sh 45678 44441

# or back
./change_server.sh 45678 33331 
#-------------------------------------------------------------------------------
# start the edge manager
sudo rm lxcList.txt ; sudp python edgeManager.py