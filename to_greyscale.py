#Import OpenCV for image processing
#Import Time
import cv2, time

# For the networking
from socket import *

import threading
import numpy as np
import pickle

IN_PORT 	= 8000

#SOCK_TYPE	= SOCK_DGRAM
SOCK_TYPE	= SOCK_STREAM

#COLUD_IP   	= "172.22.0.237"
COLUD_IP   	= "172.21.0.225"
CLOUD_PORT	= 22222
CLOUD_ADDR	= (COLUD_IP, CLOUD_PORT)

BUF_SIZE	= 1024

ORACLE_IP		= "172.22.0.31"
ORACLE_PORT		= 11111
ORACLE 			= (ORACLE_IP, ORACLE_PORT)

SERVICE_NAME = "face-process"

def get_opencv_major_version(lib=None):
    # if the supplied library is None, import OpenCV
    if lib is None:
        import cv2 as lib

    # return the major version number
    return int(lib.__version__.split(".")[0])

def is_cv2(or_better=False):
    # grab the OpenCV major version number
    major = get_opencv_major_version()

    # check to see if we are using *at least* OpenCV 2
    if or_better:
        return major >= 2

    # otherwise we want to check for *strictly* OpenCV 2
    return major == 2

def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

def receiveFrame(conn):
	length = recvall(conn,16)
	#print(length)
	stringData = recvall(conn, int(length))
	data = np.fromstring(stringData, dtype='uint8')
	frame = cv2.imdecode(data,1)

	# debug
	#cv2.imshow('GREY_SERVER',frame)
	#cv2.waitKey(10)
	return frame


def sendFrame(frame, out_sock):
	try:
		encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
		result, imgencode = cv2.imencode('.jpg', frame, encode_param)
		data = np.array(imgencode)
		stringData = data.tostring()
	except:
		print 'ERROR: Problem encoding image'
		return

	#print(str(len(stringData)).ljust(16))
	out_sock.send( str(len(stringData)).ljust(16));
	out_sock.send( stringData );

#This is the function that gets triggered when you press the button on your widget.
#The function exits when you press Q on the keyboard
def to_grey(client, addr):
	
	s = socket(AF_INET, SOCK_TYPE)
	s.connect(CLOUD_ADDR)

	while True:

		frame = receiveFrame(client)
	
		#Grayscale conversion of the frame
		grey_img = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
		
		sendFrame(grey_img, s);
	s.close()


def process(client, addr):
	s = socket(AF_INET, SOCK_TYPE)
	print 'Connect to detect:', CLOUD_ADDR
	s.connect(CLOUD_ADDR)

	faceCascade=cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
	count = 0

	while True:
		frame = receiveFrame(client)

		print("startTime: %d" % time.time())

		print 'Process Frame ', count
		count = count + 1
	
		#Grayscale conversion of the frame
		grey_img = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
		#print grey_img
		faces = faceCascade.detectMultiScale(grey_img, scaleFactor=1.05, minNeighbors=5)
		
		#superimposes a rectangle for all the detected images in the frame
		#(x,y) is the top left corner, (x+w, y+h) is the bottom right corner
		#(0,255,0) is colour green and 3 is the thickness of the rectangle edges
		for x, y, w, h in faces:
			frame = cv2.rectangle(grey_img, (x,y), (x+w, y+h), (0,255,0), 3)
			#print '%d, %d, %d, %d' % (x, y, w, h)
			#print frame

		print("endTime: %d" % time.time())

		if len(faces) == 0:
			print("No face")
		else:	
			if is_cv2():
				sendFrame(grey_img, s)
			else:
				sendFrame(frame, s)
		#print '----------------------------------------------------------------'


	s.close()	
	
def handle():
	in_sock = socket(AF_INET, SOCK_TYPE)

	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', IN_PORT))
	in_sock.listen(5)

	print('listening on port %d' % IN_PORT)
	while True:
		conn, addr = in_sock.accept()
		print 'Connection from:', addr
		#threading.Thread(target=to_grey, args=(conn,addr)).start()
		threading.Thread(target=process, args=(conn,addr)).start()

def register():
	msg_type = "register"

	entry = {}
	entry['name'] 	= SERVICE_NAME
	entry['ip'] 	= "172.22.0.31"
	entry['port']	= IN_PORT

	raw_data 		= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	#length 	= int(recv_all(s, 16))
	#addr  	= recv_all(s, length)

	s.close()
	print("register complete")
if __name__ == '__main__':
	#register()
	handle()