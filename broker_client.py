# For the networking
from socket import *
import argparse
import pickle

BROKER_IP	= "127.0.0.1"
BROKER_PORT	= 11112
BROKER		= (BROKER_IP, BROKER_PORT)
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def register(in_port, ip, port):
	msg_type = "register"

	entry = {}
	entry['msg_type']	= msg_type
	entry['in_port'] 	= in_port
	entry['out_ip'] 	= ip
	entry['out_port']	= port

	raw_data 			= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(BROKER)

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("register complete")

#-------------------------------------------------------------------------------
def remove(in_port):
	msg_type = "remove"

	entry = {}
	entry['msg_type']	= msg_type
	entry['in_port'] 	= in_port
	
	raw_data 			= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(BROKER)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.close()
	print("remove complete")
#-------------------------------------------------------------------------------
def parse_args():
	'''
	register 	'app_name' 'ipaddress' 'port number'
	lookup 		'app name'
	update 		'app_name' 'ipaddress' 'port number'
	remove 		'app_name'
	'''

	parser = argparse.ArgumentParser()
	subparsers = parser.add_subparsers(dest='func')

	reg_parser = subparsers.add_parser('register')
	reg_parser.add_argument('-i_prt', '--in_port', type=int, nargs=1, required=True)
	reg_parser.add_argument('-ip',  '--ip_address', type=str, nargs=1, required=True)
	reg_parser.add_argument('-prt', '--port', type=int, nargs=1, required=True)

	remove_parser = subparsers.add_parser('remove')
	remove_parser.add_argument('-i_prt', '--in_port', type=int, nargs=1, required=True)

	args = parser.parse_args()

	print(args)

	if args.func == 'register':
	    register(args.in_port[0], args.ip_address[0], args.port[0])
	elif args.func == 'remove':
		remove(args.name)
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	parse_args()