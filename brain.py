from socket import *
import threading
import time
import pickle

# for invoking the request to the edge manager node
import sys
sys.path.insert(0, "/home/paul/repo/edge_face_detection/enorm/CloudManager/")
from requestEdgeService import *

# for the random choice
import random

'''
The goal of this module is to be able to offload/deploy services to edge nodes
based on some set of criteria.

This is like a cloud scaler
'''
#-------------------------------------------------------------------------------
#The following are indexed by the string concat of service + location
requests		= {}	# key -> number of oracle requests in the last 
						# MEASUREMENT_THREAD_DELAY period
location 		= {}	# key -> locations
service 		= {}	# key -> services

edge_deployments	= {} # key -> deployed edge location

REPORTING_PORT 	= 55551

SERVICE_REQUEST_EJECT_THRESHOLD = 1

SERVICE_SLIDING_WINDOW = 10

# Seconds
MEASUREMENT_THREAD_DELAY = 10

# Broker information
BROKER_IP	= "127.0.0.1"
BROKER_PORT	= 11112
BROKER		= (BROKER_IP, BROKER_PORT)

# list of the different available edge locations
edge_locations = {}	 	# site name => (site_name, ip)
#-------------------------------------------------------------------------------
# Stats functions
#-------------------------------------------------------------------------------
def running_mean(x, N):
    cumsum = numpy.cumsum(numpy.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)
#-------------------------------------------------------------------------------
# Service Functions
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
def register_with_broker(in_port, ip, port):
	msg_type = "register"

	entry = {}
	entry['msg_type']	= msg_type
	entry['in_port'] 	= in_port
	entry['out_ip'] 	= ip
	entry['out_port']	= port

	raw_data 			= pickle.dumps(entry)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(BROKER)

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	s.close()
	print("register complete")
#-------------------------------------------------------------------------------
def select_location(key):
	name, entry = random.choice(list(edge_locations.items()))
	return entry
#-------------------------------------------------------------------------------
def request_service_at_edge(service, location):

	print("request the deployment of '%s' at location '%s'" % (service, location))

	# find the right request file...


	send_request(location['ip'], service + "_request.txt")
	return True
#-------------------------------------------------------------------------------
def deploy_service_to_edge(key, deploy_location):
	'''This is basically request edge service
	'''

	srv 	= service[key]
	#loc 	= location[key]
	loc 	= "TODO!!!!"



	print("deploy '%s' at '%s' -> currently '%s'" % (srv, deploy_location, loc))
	reply = request_service_at_edge(srv, deploy_location)
	if reply is None:
		print("Was unable to deploy '%s' at '%s'" % (srv, loc));
	#TODO: deploy to the edge
	register_with_broker(45678, "127.0.0.1", 44441)

	# Now record the edge site it was deployed to
	if key not in edge_deployments:
		edge_deployments[key] = []
		
	edge_deployments[key].append(deploy_location)

	print edge_deployments


#-------------------------------------------------------------------------------
# The edge device ejected 
def update_eject(service, deployed_location, key):
	print edge_deployments

	if service not in edge_deployments:
		print("ERROR: could not find any deployed service '%s'" % (service))
		print edge_deployments
		return

	if deployed_location not in edge_deployments[service]:
		print("ERROR: could not find service '%s' deployed at '%s'" % (service, deployed_location))
		print edge_deployments
		return

	print edge_deployments[service]

	print 'Redirect the broker to the cloud'
	register_with_broker(45678, "127.0.0.1", 33331)

	edge_deployments[service].remove(deployed_location)
	del edge_deployments[service]
#-------------------------------------------------------------------------------
def monitor_thread():
	while True:
		for key, value in requests.items():
			print "Monitor Thread: ", key, " ", value

			# TODO this should be a smarter check of is the service already deployed at the 
			# selected edge node
			if (value > SERVICE_REQUEST_EJECT_THRESHOLD and
				key not in edge_deployments):


				# in the future, location[key] should change to some appropriate
				# selection method
				deploy_service_to_edge(key, select_location(key))

			else:
				if key in edge_deployments:
					print("service %s already deployed " % (key))
			# rest the accounting
			requests[key] = 0
			print 'reset the value'
		time.sleep(MEASUREMENT_THREAD_DELAY)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def process_report(conn, addr):
	length 		= int(recv_all(conn, 16))
	raw_data  	= recv_all(conn, length)
	data 		= pickle.loads(raw_data)

	#print '--------------------------------------------------------------------'
	#print data
	#print '--------------------------------------------------------------------'

	if data['msg_type'] == 'oracle_report':
		key 			= data['service'] + data['location']

		if key not in requests:
			requests[key] 	= 1
		else:
			requests[key] 	+= 1
		location[key] 	= data['location']
		service[key]  	= data['service']

		# timed thread will decide if to deploy
	elif data['msg_type'] == 'eject_from_edge':
		print '--------------------------------------------------------------------'
		print data
		print '--------------------------------------------------------------------'
		# the edge kicked it out
		update_eject(data['service'], data['deploy_location'], data['key'])


	print '--------------------------------------------------------------------'
	print requests
	print edge_deployments
	print '--------------------------------------------------------------------'
#-------------------------------------------------------------------------------
def listen_to_reports():
	in_sock = socket(AF_INET, SOCK_STREAM)
	in_sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
	in_sock.bind(('', REPORTING_PORT))
	in_sock.listen(5)

	print REPORTING_PORT

	while True:
		conn, addr 	= in_sock.accept()
		#print 'accept', addr

		t 			= threading.Thread(target=process_report, args=(conn,addr))
		t.daemon 	= True
		t.start()
#-------------------------------------------------------------------------------
def start_monitor_thread():
	t 			= threading.Thread(target=monitor_thread, args=())
	t.daemon 	= True
	t.start()

#-------------------------------------------------------------------------------
def parse_arguments():
	print 'parse arguments'

	print 'TODO: read some list of locations'

	entry = {}
	entry['name'] 		= "site1"
	entry['ip']			= "127.0.0.1"

	edge_locations['site1'] = entry
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	print 'Started the cloud scaler'

	parse_arguments()

	start_monitor_thread()

	listen_to_reports()