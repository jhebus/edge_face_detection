#Import OpenCV for image processing
#Import Time
import cv2, time

# For the networking
from socket import *

from time import sleep
import numpy as np
import threading
import pickle


SERVER_IP   = "172.21.0.65"
#SERVER_IP 	= "127.0.0.1"
SERVER_PORT	= 8000
SERVER 		= (SERVER_IP, SERVER_PORT)

#SOCK_TYPE	= SOCK_DGRAM
SOCK_TYPE	= SOCK_STREAM

UUID 		= "room_1"

#ORACLE_IP		= "172.21.0.255"
ORACLE_IP		= "127.0.0.1"
ORACLE_PORT		= 11111
ORACLE 			= (ORACLE_IP, ORACLE_PORT)

SERVICE_NAME	= "face-process"
#-------------------------------------------------------------------------------
def recv_all(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf
#-------------------------------------------------------------------------------
# send the provided data to the specified addr
def sendFrame(frame, out_sock):
	''' Encode the provided image frame and send on the specified socket
	'''
	encode_param		= [int(cv2.IMWRITE_JPEG_QUALITY),90]
	result, imgencode 	= cv2.imencode('.jpg', frame, encode_param)
	data 				= np.array(imgencode)
	stringData 			= data.tostring()

	out_sock.send( str(len(stringData)).ljust(16));
	out_sock.send( stringData );

	#debug 
	#decimg=cv2.imdecode(data,1)
	#cv2.imshow('CLIENT',decimg)
	#cv2.waitKey(10)

def get_server_ip_from_oracle(service):
	''' This goes to the oracle to get the current ip for the server
	'''
	msg_type 	= "lookup"
	raw_data 	= pickle.dumps(service)

	s = socket(AF_INET, SOCK_STREAM)
	s.connect(ORACLE)

	s.send( str(len(msg_type)).ljust(16));
	s.send( msg_type );

	s.send( str(len(raw_data)).ljust(16));
	s.send( raw_data );

	length 		= int(recv_all(s, 16))
	raw_data  	= recv_all(s, length)
	server 		= pickle.loads(raw_data)
	print(server)

	s.close()

	print("Current server for '%s' is '%s'" % (service, server))
	return server

# Get the video and pass it along
def videoCollectAndSend():

	#capture video from webcam
	video = cv2.VideoCapture(0)

	count = 0

	print()
	server = get_server_ip_from_oracle(SERVICE_NAME)

	s = socket(AF_INET, SOCK_TYPE)
	s.connect(server)

	while True:
		#the read function gives two outputs. The check is a boolean function that returns if the video is being read
		check, frame = video.read()
		
		if(not check):
			printf("error reading the frame")
		else:
			sendFrame(frame, s)
			# have some time periodic 
			print("sent frame %d" % (count))
			count = count + 1

	# sleep(1.0)
	s.close()
	
	#releases the video feed from the webcam/file
	video.release()


if __name__ == '__main__':
	videoCollectAndSend();